BUILD_DIR ?= format
OBJECTS = $(BUILD_DIR)/osmformat.go $(BUILD_DIR)/fileformat.go
OSM_BINARY_REPO = https://raw.githubusercontent.com/openstreetmap/OSM-binary/master/src

all: $(OBJECTS)

%.proto:
	mkdir -p $(dir $@)
	curl -L -o $@ $(OSM_BINARY_REPO)/$(notdir $@)
	sed -i 's/option java_package.*/option go_package = ".;format";/' $@

%.go: %.proto
	protoc -I=$(BUILD_DIR) --go_out=$(BUILD_DIR) $<

clean:
	rm -rf $(BUILD_DIR)
.PHONY: clean


