module gitlab.com/evido/gosm

go 1.15

require (
	github.com/golang/protobuf v1.4.3
	github.com/neo4j/neo4j-go-driver v1.8.3
	google.golang.org/protobuf v1.25.0
)
