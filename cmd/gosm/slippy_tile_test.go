package main

import (
	"testing"
)

func getTestRoot() *SlippyTile {
	l4_9_5 := &SlippyTile{
		z: 4,
		x: 9,
		y: 5,
	}

	l3_4_2 := &SlippyTile{
		z: 3,
		x: 4,
		y: 2,
		children: [4]*SlippyTile{
			nil,
			nil,
			nil,
			l4_9_5,
		},
	}

	l2_2_1 := &SlippyTile{
		z: 2,
		x: 2,
		y: 1,
		children: [4]*SlippyTile{
			l3_4_2,
			nil,
			nil,
			nil,
		},
	}

	l1_1_0 := &SlippyTile{
		z: 1,
		x: 1,
		y: 0,
		children: [4]*SlippyTile{
			nil,
			nil,
			l2_2_1,
			nil,
		},
	}

	root := &SlippyTile{
		z: 0,
		x: 0,
		y: 0,
		children: [4]*SlippyTile{
			nil,
			l1_1_0,
			nil,
			nil,
		},
	}

	return root
}

func TestFindChild(t *testing.T) {
	root := getTestRoot()
	target := root.find(1, 1, 0)
	if target.GetCode() != "1-1-0" {
		t.Errorf("Found tile %+v\n", target)
	}
}

func TestFindGrandChild(t *testing.T) {
	root := getTestRoot()
	target := root.find(2, 2, 1)
	if target.GetCode() != "2-2-1" {
		t.Errorf("Found tile %+v\n", target)
	}
}

func TestFindGrandGrandChild(t *testing.T) {
	root := getTestRoot()
	target := root.find(3, 4, 2)
	if target.GetCode() != "3-4-2" {
		t.Errorf("Found tile %+v\n", target)
	}
}

func TestFindGrandGrandGrandChild(t *testing.T) {
	root := getTestRoot()
	target := root.find(4, 9, 5)
	if target.GetCode() != "4-9-5" {
		t.Errorf("Found tile %+v\n", target)
	}
}

func TestMergeSame(t *testing.T) {
	root := &SlippyTile{
		x:     0,
		y:     0,
		z:     0,
		count: 20,
	}

	other := &SlippyTile{
		x:     0,
		y:     0,
		z:     0,
		count: 10,
	}

	root.Merge(other)

	if root.count != 30 {
		t.Errorf("Expected target of merge to have updated counts: %d\n", root.count)
	}

	for _, child := range root.children {
		if child != nil {
			t.Errorf("Expected no childs to be added: %+v\n", root.children)
		}
	}
}

func TestMergeChild(t *testing.T) {
	root := &SlippyTile{
		x:     0,
		y:     0,
		z:     0,
		count: 20,
	}

	other := &SlippyTile{
		z:     1,
		x:     0,
		y:     1,
		count: 10,
	}

	root.Merge(other)

	if root.count != 30 {
		t.Errorf("Expected parent to be updated: %d != %d\n", root.count, 30)
	}

	if root.children[2] == nil {
		t.Errorf("Expected child to be created\n")
	}

	for ix, child := range root.children {
		if ix != 2 && child != nil {
			t.Errorf("Expected no other children to be created\n")
		}
	}

	if root.children[2].count != 10 {
		t.Errorf("Expected child to have counts: %d != %d\n", root.children[2].count, 10)
	}

	for _, grandchild := range root.children[2].children {
		if grandchild != nil {
			t.Errorf("Expected no grandchildren to be created\n")
		}
	}
}

func TestMergeGrandChild(t *testing.T) {
	root := getTestRoot()

	other := &SlippyTile{
		z:     2,
		x:     2,
		y:     1,
		count: 10,
	}

	root.Merge(other)

	if root.children[1].children[2].count != 10 {
		t.Errorf("Expected grand child to have counts: %d != %d\n",
			root.children[1].children[2].count, 10)
	}

	if root.children[1].count != 10 || root.count != 10 {
		t.Errorf("Expected parents to have counts\n")
	}
}

func TestMergeDeepChild(t *testing.T) {
	root := &SlippyTile{
		z: 0,
		x: 0,
		y: 0,
	}

	other := &SlippyTile{
		z: 6,
		x: 33,
		y: 22,
	}

	root.Merge(other)

	location := []int{1, 2, 0, 2, 2, 1}

	current := root
	for level, childIndex := range location {
		if current == nil {
			t.Errorf("Child missing at level %d\n", level)
		}

		current = current.children[childIndex]
	}
}

func TestGetTileIndex(t *testing.T) {
	root := &SlippyTile{
		x: 0,
		y: 0,
		z: 0,
	}

	index := root.getChildIndex(15, 891, 33035)

	if index != 2 {
		t.Errorf("Bounds should be clipped to 4 children: %d\n", index)
	}
}
