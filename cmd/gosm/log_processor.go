package main

import "log"

const (
	NODES_LOG_THRESHOLD = 1000000
	WAYS_LOG_THRESHOLD  = 100000
)

type LogProcessor struct {
	delegated Processor
	nodes     int
	ways      int
}

func (processor *LogProcessor) HandleNodes(nodes []Node) {
	newNodes := processor.nodes + len(nodes)
	if processor.nodes/NODES_LOG_THRESHOLD < newNodes/NODES_LOG_THRESHOLD {
		log.Printf("Processed %d nodes\n", newNodes)
	}

	processor.delegated.HandleNodes(nodes)
	processor.nodes = newNodes
}

func (processor *LogProcessor) HandleWays(ways []Way) {
	newWays := processor.ways + len(ways)
	if processor.ways/WAYS_LOG_THRESHOLD < newWays/WAYS_LOG_THRESHOLD {
		log.Printf("Processed %d ways\n", newWays)
	}

	processor.delegated.HandleWays(ways)
	processor.ways = newWays
}

func NewLogProcessor(delegated Processor) Processor {
	return &LogProcessor{
		delegated: delegated,
		nodes:     0,
		ways:      0,
	}
}
