package main

type GeohashTileFactory struct {
	codeLength int
}

func NewGeohashTileFactory(codeLength int) TileFactory {
	return &GeohashTileFactory{
		codeLength: codeLength,
	}
}

func (factory *GeohashTileFactory) NewRoot() Tile {
	return factory.FromCode("", 0)
}

func (factory *GeohashTileFactory) FromPoint(x, y float64, count int) Tile {
	return factory.FromCode(GeohashEncode(x, y, factory.codeLength), count)
}

func (factory *GeohashTileFactory) FromCode(code string, count int) Tile {
	return &GeohashTile{
		code:     code,
		count:    count,
		children: make(map[string]*GeohashTile),
	}
}
