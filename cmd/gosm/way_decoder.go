package main

import "gitlab.com/evido/gosm/format"

type Way struct {
	id    int64
	nodes []int64
	tags  map[string]string
}

type WayDecoder struct {
	block *format.PrimitiveBlock
}

func (decoder *WayDecoder) Decode(codedWay *format.Way) Way {
	nodes := decoder.DecodeNodes(codedWay.GetRefs())
	tags := decoder.DecodeTags(codedWay.GetKeys(), codedWay.GetVals())

	return Way{
		id:    codedWay.GetId(),
		nodes: nodes,
		tags:  tags,
	}
}

func (decoder *WayDecoder) DecodeTags(keys, values []uint32) map[string]string {
	tags := make(map[string]string)

	for ix := 0; ix < len(keys); ix += 1 {
		key := decoder.DecodeString(keys[ix])
		value := decoder.DecodeString(values[ix])
		tags[key] = value
	}

	return tags
}

func (decoder *WayDecoder) DecodeString(ix uint32) string {
	stringTable := decoder.block.GetStringtable()
	stringBytes := stringTable.GetS()[ix]

	return string(stringBytes)
}

func (decoder *WayDecoder) DecodeNodes(refs []int64) []int64 {
	refDecoder := DeltaDecoder{}
	nodes := make([]int64, len(refs))
	for ix, codedRef := range refs {
		nodes[ix] = refDecoder.Decode(codedRef)
	}
	return nodes
}
