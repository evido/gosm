package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
)

type GenerateTileTask struct {
	input  string
	output string
	tile   Tile
}

type GenerateTileTaskCompleted struct {
	input  string
	output string
	tile   Tile
}

func GenerateTiles(root Tile, args TileArguments) {
	controller := NewGenerateTileController(args.outputDirectory, args.maxNodes)
	controller.init()
	controller.process(root, args.inputFile)
}

type GenerateTileController struct {
	inputQueue      chan GenerateTileTask
	outputQueue     chan GenerateTileTaskCompleted
	reporter        *ProgressReporter
	maxNodes        int
	outputDirectory string
}

func NewGenerateTileController(outputDirectory string, maxNodes int) *GenerateTileController {
	inputQueue := make(chan GenerateTileTask)
	outputQueue := make(chan GenerateTileTaskCompleted)

	return &GenerateTileController{
		inputQueue:      inputQueue,
		outputQueue:     outputQueue,
		outputDirectory: outputDirectory,
		maxNodes:        maxNodes,
	}
}

func (controller *GenerateTileController) init() {
	workerCount := runtime.GOMAXPROCS(-1)
	log.Printf("Initializing %d workers\n", workerCount)
	for i := 0; i < workerCount; i++ {
		go GenerateTilesWorker(controller.inputQueue, controller.outputQueue)
	}
}

func (controller *GenerateTileController) process(root Tile, inputFile string) {
	controller.reporter = NewProgressReporter(CountTasks(root, controller.maxNodes))

	go func() {
		controller.outputQueue <- GenerateTileTaskCompleted{
			tile:   root,
			output: inputFile,
		}
	}()

	controller.writeTree(root)

	tmpFiles := make(map[string]int)
	outstanding := 1
	for completed := range controller.outputQueue {
		outstanding -= 1

		controller.reporter.tick()

		if count, ok := tmpFiles[completed.input]; ok {
			tmpFiles[completed.input] = count - 1
			if count == 1 {
				err := os.Remove(completed.input)
				if err != nil {
					log.Fatalf("Unable to delete input file: %s\n", err)
				}
				log.Printf("Removed temporary file: %s\n", path.Base(completed.input))
				delete(tmpFiles, completed.input)
			}
		}

		if completed.tile.GetCount() > controller.maxNodes {
			for _, child := range completed.tile.GetChildren() {
				filename := fmt.Sprintf("%s.osm.pbf", child.GetCode())

				var output string
				if child.GetCount() <= controller.maxNodes ||
					len(child.GetChildren()) == 0 {
					output = filepath.Join(controller.outputDirectory, filename)
				} else {
					output = filepath.Join(controller.outputDirectory, "wrk", filename)
					tmpFiles[output] = len(child.GetChildren())
				}

				controller.inputQueue <- GenerateTileTask{
					input:  completed.output,
					output: output,
					tile:   child,
				}

				outstanding += 1
			}
		}

		if outstanding == 0 {
			close(controller.outputQueue)
			close(controller.inputQueue)
		}
	}
}

func (controller *GenerateTileController) writeTree(root Tile) {
	filePath := path.Join(controller.outputDirectory, "tree.txt")
	file, err := os.Create(filePath)
	if err != nil {
		log.Fatalf("Unable to create %s: %s\n", filePath, err)
	}

	defer file.Close()

	controller.writeTile(file, root)
}

func (controller *GenerateTileController) writeTile(file *os.File, root Tile) {
	file.Write([]byte(root.GetCode()))

	if controller.maxNodes < root.GetCount() {
		children := root.GetChildren()
		if len(children) != 0 {
			file.Write([]byte("+\n"))
			for _, child := range children {
				controller.writeTile(file, child)
			}
		} else {
			file.Write([]byte("\n"))
		}
	} else {
		file.Write([]byte("\n"))
	}
}

func CountTasks(root Tile, target int) int {
	if root.GetCount() > target {
		count := 1
		for _, child := range root.GetChildren() {
			count += CountTasks(child, target)
		}
		return count
	} else {
		return 1
	}
}

func GenerateTilesWorker(inputQueue chan GenerateTileTask, outputQueue chan GenerateTileTaskCompleted) {
	for task := range inputQueue {
		completed := GenerateTileTaskCompleted{
			input:  task.input,
			output: task.output,
			tile:   task.tile,
		}

		bbox := task.tile.GetBoundingBox()
		bboxStr := fmt.Sprintf("%f,%f,%f,%f", bbox.xMin, bbox.yMin, bbox.xMax, bbox.yMax)
		cmd := exec.Command("osmconvert", task.input, "--complete-ways", "--drop-version", "--drop-author", "-b="+bboxStr, "-o="+task.output)
		if err := cmd.Run(); err != nil {
			log.Printf("Failed 'osmconvert' for %+v: %s\n", cmd, err)
		}

		go func() {
			outputQueue <- completed
		}()
	}
}
