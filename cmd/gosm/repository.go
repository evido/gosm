package main

import (
	"errors"

	"github.com/neo4j/neo4j-go-driver/neo4j"
)

const BUFFER_SIZE = 10_000

var WAY_TAGS []string = []string{
	"highway",
	"oneway",
	"junction",
	"name",
	"surface",
	"maxspeed",
	"ref",
	"lanes",
	"foot",
	"cycleway",
	"cycleway:left",
	"cycleway:both",
	"cycleway:right",
}

type Neo4jRepositoryBuffer struct {
	ways  []Way
	nodes []Node
}

type Neo4jRepository struct {
	driver  neo4j.Driver
	session neo4j.Session
	buffer  Neo4jRepositoryBuffer
}

func NewNeo4jRepository(uri, user, password string) (*Neo4jRepository, error) {
	auth := neo4j.BasicAuth(user, password, "")
	driver, err := neo4j.NewDriver(uri, auth, func(c *neo4j.Config) {
		c.Encrypted = false
	})

	if err != nil {
		return nil, err
	}

	repository := &Neo4jRepository{
		driver: driver,
		buffer: Neo4jRepositoryBuffer{
			ways:  make([]Way, 0),
			nodes: make([]Node, 0),
		},
	}

	err = repository.Initialize()

	if err != nil {
		repository.Close()
	}

	return repository, nil
}

func (repository *Neo4jRepository) Initialize() error {

	err := repository.BeginSession()

	if err != nil {
		return err
	}

	defer repository.EndSession()

	_, err = repository.session.Run(
		"CREATE CONSTRAINT uq_way_osm_id IF NOT EXISTS ON (w:Way) ASSERT w.osmId IS UNIQUE",
		map[string]interface{}{})

	_, err = repository.session.Run(
		"CREATE CONSTRAINT uq_node_osm_id IF NOT EXISTS ON (n:Node) ASSERT n.osmId IS UNIQUE",
		map[string]interface{}{})

	return nil
}

func (repository *Neo4jRepository) Close() error {
	if repository.driver == nil {
		return errors.New("Cannot close repository")
	}

	return repository.driver.Close()
}

func (repository *Neo4jRepository) BeginSession() error {
	session, err := repository.driver.Session(neo4j.AccessModeWrite)
	if err == nil {
		repository.session = session
	}
	return err
}

func (repository *Neo4jRepository) EndSession() error {
	err := repository.session.Close()
	repository.session = nil
	return err
}

func (repository *Neo4jRepository) StoreNode(node *Node) error {
	repository.buffer.nodes = append(repository.buffer.nodes, *node)
	if repository.NeedFlushBuffers() {
		return repository.FlushBuffers()
	}
	return nil
}

func (repository *Neo4jRepository) StoreWay(way *Way) error {
	repository.buffer.ways = append(repository.buffer.ways, *way)
	if repository.NeedFlushBuffers() {
		return repository.FlushBuffers()
	}
	return nil
}

func (repository *Neo4jRepository) NeedFlushBuffers() bool {
	return len(repository.buffer.nodes)+len(repository.buffer.ways) >= BUFFER_SIZE
}

func (repository *Neo4jRepository) FlushBuffers() error {
	_, err := repository.session.WriteTransaction(repository.WriteBuffers)
	repository.buffer = Neo4jRepositoryBuffer{
		ways:  make([]Way, 0),
		nodes: make([]Node, 0),
	}
	return err
}

func (repository *Neo4jRepository) WriteBuffers(transaction neo4j.Transaction) (interface{}, error) {
	repository.WriteNodes(transaction)
	repository.WriteWays(transaction)

	return nil, nil
}

func (repository *Neo4jRepository) WriteNodes(transaction neo4j.Transaction) (err error) {
	data := make([]map[string]interface{}, len(repository.buffer.nodes))

	for ix, node := range repository.buffer.nodes {
		data[ix] = map[string]interface{}{
			"osmId": node.id,
			"x":     node.x,
			"y":     node.y,
		}
	}

	_, err = transaction.Run(`UNWIND $props as properties
							  CREATE (n: Node)
							  SET n = properties, n.geom = point({ 
								  longitude : properties.x,
								  latitude  : properties.y
							  })`, map[string]interface{}{
		"props": data,
	})

	return
}

func (repository *Neo4jRepository) WriteWays(transaction neo4j.Transaction) (err error) {
	// TODO: investigate if we can create the ways and part_of relation at the same time
	err = repository.WriteWayEntities(transaction)

	if err != nil {
		return err
	}

	err = repository.WriteWayRelations(transaction)

	return
}

func (repository *Neo4jRepository) WriteWayEntities(transaction neo4j.Transaction) (err error) {
	data := make([]map[string]interface{}, len(repository.buffer.ways))

	for ix, way := range repository.buffer.ways {
		data[ix] = map[string]interface{}{
			"osmId": way.id,
		}

		for _, tag := range WAY_TAGS {
			if val, ok := way.tags[tag]; ok {
				data[ix][tag] = val
			}
		}
	}

	_, err = transaction.Run(`UNWIND $props as properties
							  CREATE (w: Way)
							  SET w = properties`,
		map[string]interface{}{
			"props": data,
		})

	return
}

func (repository *Neo4jRepository) WriteWayRelations(transaction neo4j.Transaction) (err error) {
	data := make([]map[string]interface{}, 0)

	for _, way := range repository.buffer.ways {
		for seq, nodeId := range way.nodes {
			data = append(data, map[string]interface{}{
				"wayId":  way.id,
				"nodeId": nodeId,
				"seq":    seq,
			})
		}

		if len(data) > BUFFER_SIZE {
			err = WriteWayRelation(transaction, data)
			if err != nil {
				return
			}

			data = make([]map[string]interface{}, 0)
		}
	}

	if len(data) > 0 {
		err = WriteWayRelation(transaction, data)
	}

	return
}

func WriteWayRelation(transaction neo4j.Transaction, data []map[string]interface{}) (err error) {
	_, err = transaction.Run(`
				  UNWIND $props as properties
				  MATCH (w : Way { osmId: properties.wayId }),
				        (n : Node { osmId: properties.nodeId })
				  CREATE (n)-[r : PART_OF { seq : properties.seq }]->(w)`,
		map[string]interface{}{
			"props": data,
		})

	return
}
