package main

import (
	"gitlab.com/evido/gosm/format"
)

type Node struct {
	id int64
	x  float64
	y  float64
}

const COORDINATE_PRECISION = .000000001

type DenseNodeDecoder struct {
	idDecoder  DeltaDecoder
	lonDecoder DeltaDecoder
	latDecoder DeltaDecoder
	block      *format.PrimitiveBlock
}

func (decoder *DenseNodeDecoder) Decode(codedId, codedLon, codedLat int64) Node {
	decodedId := decoder.DecodeId(codedId)
	decodedX := decoder.DecodeLon(codedLon)
	decodedY := decoder.DecodeLat(codedLat)

	return Node{
		id: decodedId,
		x:  decodedX,
		y:  decodedY,
	}
}

func (decoder *DenseNodeDecoder) DecodeId(codedId int64) int64 {
	return decoder.idDecoder.Decode(codedId)
}

func (decoder *DenseNodeDecoder) DecodeLon(codedLon int64) float64 {
	lon := decoder.lonDecoder.Decode(codedLon)
	offset := decoder.block.GetLonOffset()
	granularity := int64(decoder.block.GetGranularity())

	return COORDINATE_PRECISION * float64(offset+granularity*lon)
}

func (decoder *DenseNodeDecoder) DecodeLat(codedLat int64) float64 {
	lat := decoder.latDecoder.Decode(codedLat)
	offset := decoder.block.GetLatOffset()
	granularity := int64(decoder.block.GetGranularity())

	return COORDINATE_PRECISION * float64(offset+granularity*lat)
}
