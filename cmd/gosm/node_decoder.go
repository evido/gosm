package main

import "gitlab.com/evido/gosm/format"

type NodeDecoder struct {
	block *format.PrimitiveBlock
}

func (decoder *NodeDecoder) Decode(node *format.Node) Node {
	granularity := int64(decoder.block.GetGranularity())
	latOffset := decoder.block.GetLatOffset()
	lonOffset := decoder.block.GetLonOffset()

	return Node{
		id: node.GetId(),
		x:  COORDINATE_PRECISION * float64(lonOffset+granularity*node.GetLon()),
		y:  COORDINATE_PRECISION * float64(latOffset+granularity*node.GetLat()),
	}
}
