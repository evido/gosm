package main

import "testing"

const (
	GEOHASH_GB       = "u14dmu30"
	GEOHASH_NIL      = "s0000000"
	GEOHASH_NIL_LONG = GEOHASH_NIL + "0000"
	GEOHASH_UL       = "bpbpbpbp"
	GEOHASH_LL       = "00000000"
	GEOHASH_UR       = "zzzzzzzz"
	GEOHASH_LR       = "pbpbpbpb"
)

func TestGeohashEncodeNil(t *testing.T) {
	encoded := GeohashEncode(0., 0., 8)

	if encoded != GEOHASH_NIL {
		t.Errorf("GeohashEncode(0., 0., 8) = %s, but got %s", GEOHASH_NIL, encoded)
	}
}

func TestGeohashEncodeOverflow(t *testing.T) {
	encoded := GeohashEncode(0, 0, 12)

	if encoded != GEOHASH_NIL_LONG {
		t.Errorf("GeohashEncode(0., 0., 8) = %s, but got %s", GEOHASH_NIL_LONG, encoded)
	}
}

func TestGeohashEncode(t *testing.T) {
	encoded := GeohashEncode(3.770, 51.044, 8)

	if encoded != GEOHASH_GB {
		t.Errorf("GeohashEncode(3.770, 51.044, 8) = %s, but got %s", GEOHASH_GB, encoded)
	}
}

func TestGeohashEncodeLowerLeft(t *testing.T) {
	encoded := GeohashEncode(-180, -90, 8)

	if encoded != GEOHASH_LL {
		t.Errorf("GeohashEncode(-180, -90, 8) = %s, but got %s", GEOHASH_LL, encoded)
	}
}

func TestGeohashEncodeUpperLeft(t *testing.T) {
	encoded := GeohashEncode(-180, 90, 8)

	if encoded != GEOHASH_UL {
		t.Errorf("GeohashEncode(-180, 90, 8) = %s, but got %s", GEOHASH_UL, encoded)
	}
}

func TestGeohashEncodeUpperRight(t *testing.T) {
	encoded := GeohashEncode(180, 90, 8)

	if encoded != GEOHASH_UR {
		t.Errorf("GeohashEncode(180, 90, 8) = %s, but got %s", GEOHASH_UR, encoded)
	}
}

func TestGeohashEncodeLowerRight(t *testing.T) {
	encoded := GeohashEncode(180, -90, 8)

	if encoded != GEOHASH_LR {
		t.Errorf("GeohashEncode(180, -90, 8) = %s, but got %s", GEOHASH_LR, encoded)
	}
}

func TestGeohashDecode(t *testing.T) {
	decoded := GeohashDecode("ezs42")
	expected := BoundingBox{
		xMin: -5.625,
		yMin: 42.5830078125,
		xMax: -5.5810546875,
		yMax: 42.626953125,
	}
	if decoded != expected {
		t.Errorf("decoded: %+v\n", decoded)
	}
}

func BenchmarkGeohashEncode(b *testing.B) {
	for i := 0; i < b.N; i += 1 {
		GeohashEncode(3.770, 51.044, 8)
	}
}
