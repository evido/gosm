package main

var CODE_TABLE []rune = []rune{
	'0', '1', '2', '3', '4', '5', '6', '7',
	'8', '9', 'b', 'c', 'd', 'e', 'f', 'g',
	'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r',
	's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
}

type BoundingBox struct {
	xMin float64
	yMin float64
	xMax float64
	yMax float64
}

func findCodeIndex(targetCodeChar rune) int {
	for tableCodeIndex, tableCodeChar := range CODE_TABLE {
		if tableCodeChar == targetCodeChar {
			return tableCodeIndex
		}
	}

	return -1
}

func GeohashDecode(codeString string) BoundingBox {
	minX := -180.
	maxX := 180.
	minY := -90.
	maxY := 90.

	processingX := true
	for _, codeChar := range codeString {
		codeIndex := findCodeIndex(codeChar)

		for i := 4; i >= 0; i-- {
			b := codeIndex >> i & 0x1
			if processingX {
				if b == 1 {
					minX = (minX + maxX) / 2
				} else {
					maxX = (minX + maxX) / 2
				}
			} else {
				if b == 1 {
					minY = (minY + maxY) / 2
				} else {
					maxY = (minY + maxY) / 2
				}
			}
			processingX = !processingX
		}
	}

	return BoundingBox{
		xMin: minX,
		yMin: minY,
		xMax: maxX,
		yMax: maxY,
	}
}

func GeohashEncode(x, y float64, codeLength int) string {
	minX := -180.
	maxX := 180.
	minY := -90.
	maxY := 90.

	code := 0
	bitCodeLength := codeLength * 5
	halfBitCodeLength := bitCodeLength / 2

	for i := 0; i < halfBitCodeLength; i++ {

		midX := (minX + maxX) / 2
		bX := 0
		if midX > x {
			maxX = midX
		} else {
			bX = 1
			minX = midX
		}

		midY := (minY + maxY) / 2
		bY := 0
		if midY > y {
			maxY = midY
		} else {
			bY = 1
			minY = midY
		}

		code = (code << 2) | (bX << 1) | bY
	}

	if bitCodeLength%2 == 1 {
		b := 0

		midX := (minX + maxX) / 2
		if midX <= x {
			b = 1
		}

		code = (code << 1) | b
	}

	return base32(code, codeLength)
}

func base32(value, length int) string {
	encoded := ""

	for ix := 0; ix < length; ix++ {
		code := value & 0x1f
		encoded = string(CODE_TABLE[code]) + encoded
		value = value >> 5
	}

	return encoded
}
