package main

import (
	"log"
	"math"
	"strconv"
	"strings"
)

type SlippyTileFactory struct {
	depth int
}

func NewSlippyTileFactory(depth int) *SlippyTileFactory {
	return &SlippyTileFactory{
		depth: depth,
	}
}

func (factory *SlippyTileFactory) FromCode(code string, count int) Tile {
	// TODO: remove hack to solve negative tile codes
	zEnd := strings.Index(code, "-")
	z, err := strconv.ParseInt(code[:zEnd], 10, 32)
	if err != nil {
		log.Fatalf("Unable to parse tile code part z: %s\n", code)
	}

	xEnd := strings.Index(code[zEnd+2:], "-") + 2 + zEnd
	x, err := strconv.ParseInt(code[zEnd+1:xEnd], 10, 32)
	if err != nil {
		log.Fatalf("Unable to parse tile code part x: %s\n", code)
	}

	y, err := strconv.ParseInt(code[xEnd+1:], 10, 32)
	if err != nil {
		log.Fatalf("Unable to parse tile code part y: %s\n", code[xEnd+1:])
	}

	return factory.newSlippyTile(int(z), int(x), int(y), count)
}

func (factory *SlippyTileFactory) FromPoint(x, y float64, count int) Tile {
	n := math.Exp2(float64(factory.depth))
	tileX := int(math.Floor((x + 180.0) / 360.0 * n))
	if float64(tileX) >= n {
		tileX = int(n - 1)
	}
	tileY := int(math.Floor((1.0 - math.Log(math.Tan(y*math.Pi/180.0)+
		1.0/math.Cos(y*math.Pi/180.0))/math.Pi) /
		2.0 * n))
	return factory.newSlippyTile(factory.depth, tileX, tileY, count)
}

func (factory *SlippyTileFactory) newSlippyTile(z, x, y, count int) *SlippyTile {
	return &SlippyTile{
		x:     clip(x, 0, int(math.Exp2(float64(z)))-1),
		y:     clip(y, 0, int(math.Exp2(float64(z)))-1),
		z:     z,
		count: count,
	}
}

func (factory *SlippyTileFactory) NewRoot() Tile {
	return factory.newSlippyTile(0, 0, 0, 0)
}
