package main

type StatisticsProcessor struct {
	base         int
	nodeCountMap map[string]int
}

func NewStatsProcessor(base int) *StatisticsProcessor {
	return &StatisticsProcessor{
		base:         base,
		nodeCountMap: make(map[string]int),
	}
}

func (processor *StatisticsProcessor) HandleNodes(nodes []Node) {
	for _, node := range nodes {
		code := GeohashEncode(node.x, node.y, processor.base)
		if count, ok := processor.nodeCountMap[code]; ok {
			processor.nodeCountMap[code] = count + 1
		} else {
			processor.nodeCountMap[code] = 1
		}
	}
}

func (processor *StatisticsProcessor) HandleWays(ways []Way) {
	// ignore ways
}

func (processor *StatisticsProcessor) GetNodeCountMap() map[string]int {
	return processor.nodeCountMap
}
