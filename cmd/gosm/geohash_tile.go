package main

type GeohashTile struct {
	code     string
	count    int
	children map[string]*GeohashTile
}

func (tile *GeohashTile) GetCode() string {
	return tile.code
}

func (tile *GeohashTile) GetCount() int {
	return tile.count
}

func (tile *GeohashTile) Find(code string) Tile {
	return nil
}

func (tile *GeohashTile) GetBoundingBox() BoundingBox {
	return GeohashDecode(tile.code)
}

func (tile *GeohashTile) GetChildren() []Tile {
	tiles := make([]Tile, 0)

	for _, child := range tile.children {
		tiles = append(tiles, child)
	}

	return tiles
}

func (tile *GeohashTile) Merge(newTile Tile) {
	tile.count += newTile.GetCount()

	if len(newTile.GetCode()) > len(tile.GetCode()) {
		codeIndex := len(tile.code)
		childCode := newTile.GetCode()[codeIndex : codeIndex+1]

		if _, ok := tile.children[childCode]; !ok {
			childTile := GeohashTile{
				count:    0,
				code:     tile.code + childCode,
				children: make(map[string]*GeohashTile),
			}

			tile.children[childCode] = &childTile
		}

		tile.children[childCode].Merge(newTile)
	}
}
