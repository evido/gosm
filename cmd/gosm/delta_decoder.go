package main

type DeltaDecoder struct {
	state int64
}

func (decoder *DeltaDecoder) Decode(value int64) int64 {
	decoded := decoder.state + value
	decoder.state = decoded
	return decoded
}
