package main

import (
	"os"
	"testing"
)

const (
	ROOT        = "../.."
	EXAMPLE_PBF = ROOT + "/test/example.osm.pbf"
)

func newTestBlobReader(t *testing.T) *BlobReader {
	file, err := os.Open(EXAMPLE_PBF)

	if err != nil {
		t.Fatalf("Unable to open PBF: %v", err)
	}

	t.Cleanup(func() { file.Close() })

	return NewBlobReader(file)
}

func TestReadHeader(t *testing.T) {
	reader := newTestBlobReader(t)

	reader.Next()
}

func TestReadHeaderAndData(t *testing.T) {
	reader := newTestBlobReader(t)

	reader.Next()
	reader.Next()
}

func TestReadMultipleData(t *testing.T) {
	reader := newTestBlobReader(t)

	reader.Next()
	for i := 0; i < 5; i += 1 {
		reader.Next()
	}
}

func TestReadEOF(t *testing.T) {
	reader := newTestBlobReader(t)

	reader.Next()
	for reader.HasNext() {
		reader.Next()
	}
}
