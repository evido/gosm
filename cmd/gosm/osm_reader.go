package main

import (
	"log"

	"gitlab.com/evido/gosm/format"
)

type OsmReader struct {
	blobReader *BlobReader
	processor  Processor
}

type Processor interface {
	HandleNodes(node []Node)
	HandleWays(ways []Way)
}

func NewOsmReader(blobReader *BlobReader, processor Processor) *OsmReader {
	return &OsmReader{
		blobReader: blobReader,
		processor:  NewLogProcessor(processor),
	}
}

func (reader *OsmReader) Read() {
	for reader.blobReader.HasNext() {
		blob := reader.blobReader.Next()
		reader.processBlob(blob)
	}
}

func (reader *OsmReader) processHeaderBlock(headerBlock *format.HeaderBlock) {
	log.Printf("Processing header block %+v\n", headerBlock)
}

func (reader *OsmReader) processWays(primitiveBlock *format.PrimitiveBlock, ways []*format.Way) {
	decoder := WayDecoder{block: primitiveBlock}
	decodedWays := make([]Way, len(ways))
	for ix, codedWay := range ways {
		decodedWay := decoder.Decode(codedWay)
		decodedWays[ix] = decodedWay
	}

	reader.processor.HandleWays(decodedWays)
}

func (reader *OsmReader) processDenseNodes(primitiveBlock *format.PrimitiveBlock,
	denseNodes *format.DenseNodes) {

	decoder := DenseNodeDecoder{
		block: primitiveBlock,
	}

	decodedNodes := make([]Node, len(denseNodes.GetId()))
	for ix, id := range denseNodes.GetId() {
		lon := denseNodes.GetLon()[ix]
		lat := denseNodes.GetLat()[ix]
		decodedNode := decoder.Decode(id, lon, lat)
		decodedNodes[ix] = decodedNode
	}

	reader.processor.HandleNodes(decodedNodes)
}

func (reader *OsmReader) processNodes(primitiveBlock *format.PrimitiveBlock,
	nodes []*format.Node) {
	decodedNodes := make([]Node, len(nodes))

	decoder := NodeDecoder{
		block: primitiveBlock,
	}

	for ix, codedNode := range nodes {
		decodedNode := decoder.Decode(codedNode)
		decodedNodes[ix] = decodedNode
	}

	reader.processor.HandleNodes(decodedNodes)
}

func (reader *OsmReader) processPrimitiveBlock(primitiveBlock *format.PrimitiveBlock) {
	for _, primitiveGroup := range primitiveBlock.GetPrimitivegroup() {
		if nodesLen := len(primitiveGroup.GetNodes()); nodesLen > 0 {
			// log.Printf("Found %d nodes\n", nodesLen)
			reader.processNodes(primitiveBlock, primitiveGroup.GetNodes())
		}

		if waysLen := len(primitiveGroup.GetWays()); waysLen > 0 {
			// log.Printf("Found %d ways \n", waysLen)
			reader.processWays(primitiveBlock, primitiveGroup.GetWays())
		}

		if relationsLen := len(primitiveGroup.GetRelations()); relationsLen > 0 {
			// log.Printf("Found %d relations\n", relationsLen)
		}

		if changeSetsLen := len(primitiveGroup.GetChangesets()); changeSetsLen > 0 {
			// log.Printf("Found %d changesets\n", changeSetsLen)
		}

		if primitiveGroup.GetDense() != nil {
			// log.Printf("Found %d dense nodes\n", len(primitiveGroup.GetDense().GetId()))
			reader.processDenseNodes(primitiveBlock, primitiveGroup.GetDense())
		}
	}
}

func (reader *OsmReader) processBlob(blob interface{}) {
	switch blob.(type) {
	case *format.HeaderBlock:
		reader.processHeaderBlock(blob.(*format.HeaderBlock))
	case *format.PrimitiveBlock:
		reader.processPrimitiveBlock(blob.(*format.PrimitiveBlock))
	default:
		log.Printf("Received blob with type unknown type\n")
	}
}
