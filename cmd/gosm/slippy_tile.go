package main

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"
)

type SlippyTile struct {
	x, y, z  int
	count    int
	children [4]*SlippyTile
}

func (tile *SlippyTile) GetCount() int {
	return tile.count
}

func (tile *SlippyTile) GetCode() string {
	return fmt.Sprintf("%d-%d-%d", tile.z, tile.x, tile.y)
}

func (tile *SlippyTile) GetChildren() []Tile {
	children := make([]Tile, 0)

	hasChildren := false
	for _, child := range tile.children {
		if child != nil {
			hasChildren = true
			break
		}
	}

	if hasChildren {
		for ix, child := range tile.children {
			if child != nil {
				children = append(children, child)
			} else {
				newChild := tile.newChildAt(ix)
				children = append(children, newChild)
			}
		}
	}

	return children
}

func (tile *SlippyTile) GetBoundingBox() BoundingBox {
	return BoundingBox{
		xMin: tile2Lon(tile.x, tile.z),
		yMin: tile2Lat(tile.y+1, tile.z),
		xMax: tile2Lon(tile.x+1, tile.z),
		yMax: tile2Lat(tile.y, tile.z),
	}
}

func tile2Lon(x, z int) float64 {
	return float64(x)/math.Exp2(float64(z))*360.0 - 180.0
}

func tile2Lat(y, z int) float64 {
	n := math.Pi - 2.0*math.Pi*float64(y)/math.Exp2(float64(z))
	return 180.0 / math.Pi * math.Atan(0.5*(math.Exp(n)-math.Exp(-n)))
}

func (tile *SlippyTile) Merge(other Tile) {
	otherSlippyTile := other.(*SlippyTile)
	tile.merge(otherSlippyTile.z, otherSlippyTile.x, otherSlippyTile.y, other.GetCount())
}

func (tile *SlippyTile) merge(z, x, y, count int) {
	tile.count += count

	if z > tile.z {
		child := tile.getChild(z, x, y)
		child.merge(z, x, y, count)
	}
}

func (tile *SlippyTile) getChild(z, x, y int) *SlippyTile {
	childIndex := tile.getChildIndex(z, x, y)
	child := tile.children[childIndex]
	if child == nil {
		child = tile.newChildAt(childIndex)
		tile.children[childIndex] = child
	}
	return child
}

func (tile *SlippyTile) newChildAt(index int) *SlippyTile {
	return &SlippyTile{
		z:     tile.z + 1,
		x:     2*tile.x + index%2,
		y:     2*tile.y + index/2,
		count: 0,
	}
}

func (tile *SlippyTile) Find(code string) Tile {
	z, x, y := parseSlippyTileCode(code)
	return tile.find(z, x, y)
}

func parseSlippyTileCode(code string) (int, int, int) {
	parts := strings.Split(code, "-")

	z, err := strconv.ParseInt(parts[0], 10, 32)
	if err != nil {
		log.Fatalf("Unable to parse code: %s\n", err)
	}

	x, err := strconv.ParseInt(parts[1], 10, 32)
	if err != nil {
		log.Fatalf("Unable to parse code: %s\n", err)
	}

	y, err := strconv.ParseInt(parts[2], 10, 32)
	if err != nil {
		log.Fatalf("Unable to parse code: %s\n", err)
	}

	return int(z), int(x), int(y)
}

func (tile *SlippyTile) find(z, x, y int) Tile {
	if tile.z == z {
		return tile
	}

	childIndex := tile.getChildIndex(z, x, y)
	target := tile.children[childIndex]
	if target == nil {
		return nil
	}

	return target.find(z, x, y)
}

func (tile *SlippyTile) getChildIndex(z, x, y int) int {
	factor := int(math.Exp2(float64(z) - float64(tile.z) - 1))
	childX := x / factor
	childY := y / factor

	index := 2*clip(childY-2*tile.y, 0, 1) + clip(childX-2*tile.x, 0, 1)

	return index
}

func clip(val, min, max int) int {
	if val < min {
		return min
	}

	if val > max {
		return max
	}

	return val
}
