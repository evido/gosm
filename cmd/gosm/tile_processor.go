package main

import (
	"encoding/json"
	"log"
	"os"
)

type TileProcessor struct {
	tileFactory TileFactory
	tileMap     map[string]Tile
}

type TileFactory interface {
	NewRoot() Tile
	FromCode(code string, count int) Tile
	FromPoint(x, y float64, count int) Tile
}

func NewTileProcessor(tileFactory TileFactory) *TileProcessor {
	return &TileProcessor{
		tileMap:     make(map[string]Tile),
		tileFactory: tileFactory,
	}
}

func (processor *TileProcessor) Build(inputFile string) {
	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalf("Unable to read file: %s\n", err)
	}
	defer file.Close()

	blobReader := NewBlobReader(file)
	osmReader := NewOsmReader(blobReader, processor)

	osmReader.Read()
	log.Printf("Processed PBF file %s", inputFile)
}

func (processor *TileProcessor) HandleNodes(nodes []Node) {
	for _, node := range nodes {
		newTile := processor.tileFactory.FromPoint(node.x, node.y, 1)
		code := newTile.GetCode()
		if tile, ok := processor.tileMap[code]; ok {
			tile.Merge(newTile)
		} else {
			processor.tileMap[code] = newTile
		}
	}
}

func (processor *TileProcessor) HandleWays(ways []Way) {
	// ignore ways
}

func (processor *TileProcessor) ReadTiles(tileCountsPath string) {
	reader, err := os.Open(tileCountsPath)
	if err != nil {
		log.Fatalf("Unable to open file: %s\n", err)
	}

	defer reader.Close()
	decoder := json.NewDecoder(reader)

	var tileDtos []TileDto
	err = decoder.Decode(&tileDtos)

	if err != nil {
		log.Fatalf("Unable to read tiles: %s\n", err)
	}

	processor.tileMap = make(map[string]Tile)
	for _, tileDto := range tileDtos {
		tile := processor.tileFactory.FromCode(tileDto.Code, tileDto.Count)
		processor.tileMap[tile.GetCode()] = tile
	}
}

func (processor *TileProcessor) WriteTiles(tileCountsPath string) {
	tileDtos := make([]TileDto, 0)

	for _, tile := range processor.tileMap {
		tileDto := TileDto{
			Code:  tile.GetCode(),
			Count: tile.GetCount(),
		}

		tileDtos = append(tileDtos, tileDto)
	}

	writer, err := os.Create(tileCountsPath)
	if err != nil {
		log.Fatalf("Unable to create file: %s\n", err)
	}

	defer writer.Close()

	encoder := json.NewEncoder(writer)
	err = encoder.Encode(tileDtos)
	if err != nil {
		log.Fatalf("Unable to write tiles: %s\n", err)
	}
}

func (processor *TileProcessor) GetTree() Tile {
	root := processor.tileFactory.NewRoot()

	for _, tile := range processor.tileMap {
		root.Merge(tile)
	}

	return root
}
