package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

const (
	TILE_COUNTS_PATH = "wrk/tiles.json"
)

type Tile interface {
	Find(code string) Tile
	GetCode() string
	GetCount() int
	GetChildren() []Tile
	GetBoundingBox() BoundingBox
	Merge(other Tile)
}

type TileStrategyProcessor interface {
	GetTree() Tile
	Build(inputFile string)
	WriteTiles(path string)
	ReadTiles(path string)
}

type TileDto struct {
	Code  string `json:"code"`
	Count int    `json:"count"`
}

func BuildTiles(args TileArguments) Tile {
	tilesPath := filepath.Join(args.outputDirectory, TILE_COUNTS_PATH)
	strategy := NewTileStrategyProcessor(args.strategy)
	if _, err := os.Stat(tilesPath); os.IsNotExist(err) {
		log.Printf("Unable to find cached counts in %s", tilesPath)
		os.MkdirAll(filepath.Dir(tilesPath), os.ModePerm)
		strategy.Build(args.inputFile)
		strategy.WriteTiles(tilesPath)
	} else {
		log.Printf("Found cached counts in %s", tilesPath)
		strategy.ReadTiles(tilesPath)
	}

	return strategy.GetTree()
}

func NewTileStrategyProcessor(name string) TileStrategyProcessor {
	nameParts := strings.Split(name, "@")
	depth, err := strconv.ParseInt(nameParts[1], 10, 32)

	if err != nil {
		log.Fatalf("Unknown strategy name: %s\n", name)
	}

	switch nameParts[0] {
	case "geohash":
		tileFactory := NewGeohashTileFactory(int(depth))
		return NewTileProcessor(tileFactory)
	case "slippy":
		tileFactory := NewSlippyTileFactory(int(depth))
		return NewTileProcessor(tileFactory)
	default:
		log.Fatalf("Unknown strategy name: %s\n", name)
		return nil
	}
}

func FindTiles(root Tile, target int) {
	if root.GetCount() > target {
		if len(root.GetChildren()) > 0 {
			for _, tile := range root.GetChildren() {
				FindTiles(tile, target)
			}
		} else {
			fmt.Printf("%s %d\n", root.GetCode(), root.GetCount())
		}
	} else {
		fmt.Printf("%s %d\n", root.GetCode(), root.GetCount())
	}
}
