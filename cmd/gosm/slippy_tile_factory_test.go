package main

import "testing"

func TestSlippyTileCodesWithNegativeIndiciesCanBeParsed(t *testing.T) {
	code := "15--3000--2000"
	factory := NewSlippyTileFactory(15)
	tile := factory.FromCode(code, 1000).(*SlippyTile)

	if tile.x != 0 && tile.y != 0 {
		t.Errorf("Expected tile code (15, 0, 0) for %+v\n", tile)
	}
}

func TestSlippyTileCodesCanBeParsed(t *testing.T) {
	code := "1-0-1"
	factory := NewSlippyTileFactory(15)
	tile := factory.FromCode(code, 1000).(*SlippyTile)

	if tile.x != 0 && tile.y != 1 {
		t.Errorf("Expected tile code (1, 0, 1) for %+v\n", tile)
	}
}
