package main

import (
	"bufio"
	"bytes"
	"compress/zlib"
	"encoding/binary"
	"fmt"
	"github.com/golang/protobuf/proto"
	"gitlab.com/evido/gosm/format"
	"io"
	"os"
)

const (
	HEADER_LENGTH_SIZE = 4
)

type BlobReader struct {
	reader *bufio.Reader
}

func NewBlobReader(file *os.File) *BlobReader {
	return &BlobReader{
		reader: bufio.NewReader(file),
	}
}

func (reader *BlobReader) ReadBytes(wantedNum int) []byte {
	buffer := make([]byte, wantedNum)
	readNum, err := io.ReadFull(reader.reader, buffer)

	if wantedNum != readNum {
		panic(fmt.Errorf("Read error wanted %d bytes but received %d", wantedNum, readNum))
	}

	if err != nil {
		panic(fmt.Errorf("Read error: %v", err))
	}

	return buffer
}

func (reader *BlobReader) ReadBlobHeaderLength() int {
	headerLengthBytes := reader.ReadBytes(HEADER_LENGTH_SIZE)
	return int(binary.BigEndian.Uint32(headerLengthBytes))
}

func (reader *BlobReader) ReadBlobHeader() *format.BlobHeader {
	headerLength := reader.ReadBlobHeaderLength()
	headerBytes := reader.ReadBytes(headerLength)

	blobHeader := &format.BlobHeader{}
	if err := proto.Unmarshal(headerBytes, blobHeader); err != nil {
		panic(fmt.Errorf("Failed to parse blob header: %v", err))
	}

	return blobHeader
}

func (reader *BlobReader) ReadBlob(blobHeader *format.BlobHeader) *format.Blob {
	blobSize := int(blobHeader.GetDatasize())
	blobBytes := reader.ReadBytes(blobSize)

	blob := &format.Blob{}
	if err := proto.Unmarshal(blobBytes, blob); err != nil {
		panic(fmt.Errorf("Failed to parse blob: %v", err))
	}

	return blob
}

func GetBytes(blob *format.Blob) []byte {
	blobBytesReader := bytes.NewReader(blob.GetZlibData())
	blobZlibReader, err := zlib.NewReader(blobBytesReader)

	if err != nil {
		panic(fmt.Errorf("Unabled to decompress blob: %v", err))
	}

	uncompressedBytes := make([]byte, blob.GetRawSize())
	io.ReadFull(blobZlibReader, uncompressedBytes)

	return uncompressedBytes
}

func (reader *BlobReader) Next() interface{} {
	header := reader.ReadBlobHeader()
	blob := reader.ReadBlob(header)
	blobBytes := GetBytes(blob)

	var token proto.Message
	switch *header.Type {
	case "OSMHeader":
		token = &format.HeaderBlock{}
	case "OSMData":
		token = &format.PrimitiveBlock{}
	default:
		panic(fmt.Errorf("Unknown type: %v", header.Type))
	}

	if err := proto.Unmarshal(blobBytes, token); err != nil {
		panic(fmt.Errorf("Failed to parse OSM Header: %v", err))
	}

	return token
}

func (reader *BlobReader) HasNext() bool {
	_, err := reader.reader.Peek(HEADER_LENGTH_SIZE)

	if err == nil {
		return true
	}

	if err != io.EOF {
		panic(fmt.Errorf("unexpected peek error: %v", err))
	}

	return false
}
