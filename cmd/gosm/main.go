package main

import (
	"log"
	"os"
	"strconv"
)

const (
	INPUT_FILE_ARG_IX = iota
	TILE_STRATEGY_ARG_IX
	TARGET_ARG_IX
	OUTPUT_DIRECTORY_ARG_IX
)

type TileArguments struct {
	inputFile       string
	strategy        string
	maxNodes        int
	outputDirectory string
}

func NewTileArguments(args []string) TileArguments {
	target, err := strconv.ParseInt(args[TARGET_ARG_IX], 10, 32)
	if err != nil {
		log.Fatalf("Invalid target: %s, %s\n", args[TARGET_ARG_IX], err)
	}

	return TileArguments{
		inputFile:       args[INPUT_FILE_ARG_IX],
		strategy:        args[TILE_STRATEGY_ARG_IX],
		maxNodes:        int(target),
		outputDirectory: args[OUTPUT_DIRECTORY_ARG_IX],
	}
}

func DisplayStats(args []string) {
	parsedArgs := NewTileArguments(args)
	root := BuildTiles(parsedArgs)
	FindTiles(root, parsedArgs.maxNodes)
}

func SplitTiles(args []string) {
	parsedArgs := NewTileArguments(args)
	root := BuildTiles(parsedArgs)
	GenerateTiles(root, parsedArgs)
}

func ExportWays(args []string) {
	file, err := os.Open(args[INPUT_FILE_ARG_IX])
	if err != nil {
		panic(err)
	}
	defer file.Close()

	blobReader := NewBlobReader(file)
	wayProcessor := NewWayProcessor()
	osmReader := NewOsmReader(blobReader, wayProcessor)

	osmReader.Read()
	wayProcessor.Report()
	repository, err := NewNeo4jRepository(args[1], args[2], args[3])

	if err != nil {
		panic(err)
	}

	defer repository.Close()
	wayProcessor.Export(repository)
}

func main() {
	cmd := os.Args[1]
	remaining := os.Args[2:]

	switch cmd {
	case "export":
		ExportWays(remaining)
		break
	case "stats":
		DisplayStats(remaining)
		break
	case "split":
		SplitTiles(remaining)
		break
	default:
		log.Fatalf("Unknown command: %s\n", cmd)
	}

}
