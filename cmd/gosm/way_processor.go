package main

import "log"

type WayProcessor struct {
	wayMap  map[int64]Way
	nodeMap map[int64]Node
}

func NewWayProcessor() *WayProcessor {
	return &WayProcessor{
		wayMap:  make(map[int64]Way),
		nodeMap: make(map[int64]Node),
	}
}

func (processor *WayProcessor) HandleWays(ways []Way) {
	for _, way := range ways {
		processor.wayMap[way.id] = way
	}
}

func (processor *WayProcessor) HandleNodes(nodes []Node) {
	for _, node := range nodes {
		processor.nodeMap[node.id] = node
	}
}

func (wayProcessor *WayProcessor) Report() {
	log.Printf("Ways: %d\n", len(wayProcessor.wayMap))

	highWays := 0
	for _, way := range wayProcessor.wayMap {
		if _, ok := way.tags["highway"]; ok {
			highWays += 1
		}
	}

	log.Printf("Highways: %d\n", highWays)
}

func (wayProcessor *WayProcessor) Export(repository *Neo4jRepository) {

	err := repository.BeginSession()
	if err != nil {
		panic(err)
	}
	defer repository.EndSession()

	for _, node := range wayProcessor.nodeMap {
		err = repository.StoreNode(&node)
		if err != nil {
			panic(err)
		}
	}

	for _, way := range wayProcessor.wayMap {
		if _, ok := way.tags["highway"]; ok {
			err = repository.StoreWay(&way)
			if err != nil {
				panic(err)
			}
		}
	}

	repository.FlushBuffers()
}
