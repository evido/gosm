package main

import "log"

type ProgressReporter struct {
	reportStep int
	total      int
	progress   int
}

func NewProgressReporter(total int) *ProgressReporter {
	reportStep := 1
	if total >= 100 {
		reportStep = total / 100
	}

	return &ProgressReporter{
		reportStep: reportStep,
		total:      total,
		progress:   0,
	}
}

func (reporter *ProgressReporter) tick() {
	reporter.progress += 1
	if reporter.progress%reporter.reportStep == 0 {
		log.Printf("Completed %d out of %d tasks.\n", reporter.progress, reporter.total)
	}
}
